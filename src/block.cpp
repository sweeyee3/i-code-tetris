#include "block.h"
#include "block_container.h"

void Block::translate(Coord move)
{
    position.x += move.x;
    position.y += move.y;    
}

void Block::rotate(int direction)
{
    orient += direction;
    if (orient < 0) orient = 0;
    orient = orient % SHAPE_ORIENTATION;
}

bool Block::overlap(int x, int y)
{    
    if (x < position.x || x >= position.x + SHAPE_WIDTH || y < position.y || y > position.y + SHAPE_HEIGHT)
    {
        return false;
    }

    Coord bc[4] = { {-1, -1} };
    get_block_coord(bc);
    for (int i=0; i<4; i++)
    {
        if (bc[i].x == x && bc[i].y == y)
        {
            return true;
        }
    }

    return true;    
}

void Block::get_block_coord(Coord coord[4])
{    
    int idx = 0;
    for (int i=0; i<SHAPE_HEIGHT; i++)
    {
        for (int j=0; j<SHAPE_WIDTH; j++)
        {
            if (SHAPES[shape][orient][i][j] == 1) // look in  the 4x4 to find the parts which are marked as 1
            {
                // printf("i: %i, j: %i\n", i, j);                
                int px = position.x + j;
                int py = position.y + i;
                coord[idx].x = px;
                coord[idx].y = py;
                idx++;
            }         
        }
    }    
}