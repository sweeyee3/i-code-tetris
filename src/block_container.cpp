#include "block_container.h"
#include "block.h"
#include <random>
#include "timer.h"

using namespace std;

void BlockContainer::spawn_block() 
{
    if (!gameOver)
    {
        std::random_device rd; // obtain a random number from hardware
        std::mt19937 gen(rd()); // seed the generator
        std::uniform_int_distribution<> distr_shape_num(0, SHAPE_NUM - 1); // define the range
        std::uniform_int_distribution<> distr_shape_orient(0, SHAPE_ORIENTATION - 1); // define the range

        int shape = distr_shape_num(gen);
        // int shape = 0;
        int orientation = distr_shape_orient(gen);    
        // int orientation = 0;    
        Coord position = {(MAX_CONTAINER_WIDTH)/2, 0};
        if (current == NULL && next == NULL)
        {        
            current = new Block( shape, orientation, position );                                

            Coord pos_indices[4] = { {-1, -1} };
            current->get_block_coord(pos_indices);
            for(int i=0; i<4; i++)
            {            
                if (container[pos_indices[i].y][pos_indices[i].x] == 0)
                {
                    container[pos_indices[i].y][pos_indices[i].x] = 1;
                }                        
            }

            // generate next shape
            shape = distr_shape_num(gen);
            orientation = distr_shape_orient(gen);

            next = new Block( shape, orientation );                                   
        }
        else
        {        
            current->shape = next->shape;
            current->orient = next->orient;
            current->position = position;

            next->shape = shape;
            next->orient = orientation;

            if (is_overlap(current))
            {
                printf("game over\n");
                gameOver = true;            
            }
        }
    }             
}

void BlockContainer::find_final_pos()
{
    int height_pos = 0;
    for (int i=CONTAINER_HEIGHT-1; i>=0; i--)
    {
        for (int j=0; j<CONTAINER_WIDTH; j++)
        {
            if (current->overlap(i, j))
            {
                height_pos = i;
                break;
            }
        }                                        
    }
    
    Coord block_coord[4] = {{ -1, -1 }};
    current->get_block_coord(block_coord);
    for (int i =0; i<4; i++)
    {
        if (block_coord[i].x >= 0 && block_coord[i].y >= 0)
        {
            container[block_coord[i].y][block_coord[i].x] = 1; // set final position here
        }        
    }
}

void BlockContainer::set_final_pos()
{

}

bool BlockContainer::update_container(int flag)
{
    Coord pos_indices[4] = { {-1, -1} };
    current->get_block_coord(pos_indices);    

    // update by the flag
    for(int i=0; i<4; i++)
    {
        container[pos_indices[i].y][pos_indices[i].x] = flag;                
    }

    return true;
}

void BlockContainer::check_clear_row()
{
    for (int i=0; i<CONTAINER_HEIGHT; i++)
    {
        int total = 0;
        for (int j=0; j<CONTAINER_WIDTH; j++)
        {
            total += container[i][j];
        }

        if (total == CONTAINER_WIDTH)
        {            
            // set row to 0
            for (int j=0; j<CONTAINER_WIDTH; j++)
            {
                container[i][j] = 0;                
            }
            printf("clear: %i\n", i);
            clear[i] = 1;                                    
        }
    }
}

void BlockContainer::move_rows()
{
    for (int i=0; i<CONTAINER_HEIGHT; i++)
    {
        if (clear[i] == 1)
        {            
            for (int j=i; j>0; j--)
            {                            
                move_one_row(j-1, j);
            }
        }
    }

    // reset clear status
    for (int i=0 ;i<CONTAINER_HEIGHT; i++)
    {
        clear[i] = 0;
    }

    // printf("from: %i, to: %i\n", from, to);    
}

void BlockContainer::move_one_row(int from, int to)
{    
    if (from < 0)
    {        
        for (int i = 0; i<CONTAINER_WIDTH; i++)
        {
            container[to][i] = 0;
        }
    }
    else
    {        
        for (int i = 0; i<CONTAINER_WIDTH; i++)
        {
            // printf("from: %i, to: %i, value: container[\n", from, to);                        
            container[to][i] = container[from][i];            
        }
    }    
}

void BlockContainer::right()
{
    // reset previous position
    update_container(0);

    // move object            
    current->position.x += 1;

    if (!is_out_upper_x_bounds(current) && !is_overlap(current))
    {
        update_container(1);
    }
    else
    {
        current->position.x -= 1;
        update_container(1);
    }             
}

void BlockContainer::left()
{
    // reset previous position
    update_container(0);

    // move object            
    current->position.x -= 1;

    if (!is_out_lower_x_bounds(current) && !is_overlap(current))
    {
        update_container(1);
    }
    else
    {
        current->position.x += 1;
        update_container(1);
    }
}

void BlockContainer::rotate(int direction)
{
    // reset previous position
    update_container(0);
    current->rotate(direction);
    // check if out of bounds. if so, move block to corrected position
    while (is_out_upper_x_bounds(current))
    {
        current->position.x -= 1;
    }

    if (is_out_lower_x_bounds(current))
    {
        current->position.x += 1;
    }

    update_container(1);                     
}

void BlockContainer::speed(int newSpeed)
{
    if (newSpeed > 0)
    {
        speedTimer = newSpeed;
    }
    else
    {
        speedTimer = MOVE_TIMER;
    }    
}

bool BlockContainer::is_overlap(Block* block)
{
    Coord pos_indices[4] = { {-1, -1} };
    block->get_block_coord(pos_indices);

    for(int i=0; i<4; i++)
    {
        if (container[pos_indices[i].y][pos_indices[i].x] == 1) return true;                
    }
    
    return false;
}

bool BlockContainer::is_out_upper_x_bounds(Block* block)
{
    Coord pos_indices[4] = { {-1, -1} };
    block->get_block_coord(pos_indices);

    for(int i=0; i<4; i++)
    {
        if (pos_indices[i].x >= CONTAINER_WIDTH) return true;
    }        

    return false;
}

bool BlockContainer::is_out_upper_y_bounds(Block* block)
{
    Coord pos_indices[4] = { {-1, -1} };
    block->get_block_coord(pos_indices);

    for(int i=0; i<4; i++)
    {
        if (pos_indices[i].y >= CONTAINER_HEIGHT) return true;
    }    

    return false;
}

bool BlockContainer::is_out_lower_x_bounds(Block* block)
{
    Coord pos_indices[4] = { {-1, -1} };
    block->get_block_coord(pos_indices);

    for(int i=0; i<4; i++)
    {
        if (pos_indices[i].x < 0) return true;
    }    

    return false;
}

bool BlockContainer::is_out_lower_y_bounds(Block* block)
{
    Coord pos_indices[4] = { {-1, -1} };
    block->get_block_coord(pos_indices);

    for(int i=0; i<4; i++)
    {
        if (pos_indices[i].y < 0) return true;
    }

    return false;
}

void BlockContainer::initialize(int width, int height)
{
    screenWidth = width;
    screenHeight = height;
    speedTimer = MOVE_TIMER;
    gameOver = false;

    for (int i=0; i<CONTAINER_HEIGHT; i++) // set clear to 0
    {
        clear[i] = 0;
    }

    // for (int i=0; i<CONTAINER_HEIGHT; i++)
    // {
    //     for (int j=0; j<CONTAINER_WIDTH; j++)
    //     {
    //         container[i][j] = 1;
    //     }
    // }
    spawn_block();
    updateTimer = Timer::instance()->getTicks() + speedTimer;    
}

void BlockContainer::update()
{
    if (current != NULL)
    {                
        if (Timer::instance()->isTimePassed(Timer::instance()->getTicks(), updateTimer))
        {
            // reset timer
            updateTimer = Timer::instance()->getTicks() + speedTimer;            

            // reset previous position
            update_container(0);

            // move object            
            current->position.y += 1;
            // printf("y position: %i\n", current->position.y);

            if (!is_out_lower_y_bounds(current))
            {
                if (!is_out_upper_y_bounds(current) && !is_overlap(current))
                {
                    // update next coords                    
                    update_container(1);
                }
                else
                {                                        
                    current->position.y -= 1;
                    // printf("final y position: %i\n", current->position.y);
                    // set permanent coords
                    update_container(1);

                    // check clear row and move
                    check_clear_row();

                    // move cleared row
                    move_rows();
                    
                    // spawn block
                    spawn_block();
                }
                
            }            
        }                                                                
    }
}

void BlockContainer::render(SDL_Renderer* renderer)
{
    // render score


    // render container
    int y_pos = ((screenHeight / 16) - CONTAINER_HEIGHT);
    int x_pos = ((screenWidth / 16) - CONTAINER_WIDTH)/2;    

    for (int i=0; i<CONTAINER_HEIGHT; i++)
    {
        for (int j=0; j<CONTAINER_WIDTH; j++)
        {
            if (container[i][j] != 0)
            {
                Loader::instance()->textureWrapperMap["resources/img/blocks.png"]->render(0, SDL_FLIP_NONE, false);
                Loader::instance()->textureWrapperMap["resources/img/blocks.png"]->mPosition.y = ((i + y_pos) * 16);
                Loader::instance()->textureWrapperMap["resources/img/blocks.png"]->mPosition.x = ((j + x_pos) * 16);

                if (container[i][j] == 1)
                {
                    // draw block                
                    Loader::instance()->textureWrapperMap["resources/img/blocks.png"]->setSpriteClipIndex(0);
                }
                else if (container[i][j] == 2)
                {               
                    Loader::instance()->textureWrapperMap["resources/img/blocks.png"]->setSpriteClipIndex(1);
                }
            }                    
        }
    }

    // render next block
    x_pos = ((screenWidth / 16)/4) * 3;
    y_pos = ((screenHeight / 16)/4);        
    Coord pos_indices[4] = { {-1, -1} };
    next->get_block_coord(pos_indices);
    for (int i=0; i<4; i++)
    {
        Loader::instance()->textureWrapperMap["resources/img/blocks.png"]->render(0, SDL_FLIP_NONE, false);
        Loader::instance()->textureWrapperMap["resources/img/blocks.png"]->mPosition.y = ((pos_indices[i].y + y_pos) * 16);
        Loader::instance()->textureWrapperMap["resources/img/blocks.png"]->mPosition.x = ((pos_indices[i].x + x_pos) * 16);            
        Loader::instance()->textureWrapperMap["resources/img/blocks.png"]->setSpriteClipIndex(1);
    }   
}