#include "my_util.h"
using namespace my_util;

void my_util::print_v_s(vector<string> a)
{
    printf("string vec: ");
    for(int i=0;i<a.size();i++)
    {
        printf("%s, ", a[i].c_str());
    }
    printf("\n");
}
void my_util::print_v_i(vector<int> a)
{
    printf("int vec: ");
    for(int i=0;i<a.size();i++)
    {
        printf("%i,", a[i]);
    }
    printf("\n");
}
void my_util::print_v_f(vector<float> a)
{
    printf("float vec: ");
    for(int i=0;i<a.size();i++)
    {
        printf("%f,", a[i]);
    }
    printf("\n");
}
void my_util::print_v_d(vector<double> a)
{
    printf("double vec: ");
    for(int i=0;i<a.size();i++)
    {
        printf("%f,", a[i]);
    }
    printf("\n");
}
void my_util::print_v_c(vector<char> a)
{
    printf("char vec: ");
    for(int i=0;i<a.size();i++)
    {
        printf("%c,", a[i]);
    }
    printf("\n");
}

vector<string> my_util::splitBy(string line, char delimiter)
{
    // split text by comma, save to map
    string token;
    stringstream ss(line);
    vector<string> splitStrings;
    while(getline(ss, token, delimiter))
    {
        splitStrings.push_back(token);
    }
    return splitStrings;
}