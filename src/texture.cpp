
#include "texture.h"
using namespace std;

bool Texture::loadFromRenderedText( SDL_Renderer* sdl_renderer, TTF_Font* font, string font_path, int index, string textureText, SDL_Color textColor )
{
    //Get rid of preexisting texture
    free();

    //Render text surface
    SDL_Surface* textSurface = TTF_RenderText_Solid( font, textureText.c_str(), textColor );
    if( textSurface == NULL )
    {
        printf( "Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError() );
    }
    else
    {		
		// set renderer
        renderer = sdl_renderer; 

        //Create texture from surface pixels
        mTexture = SDL_CreateTextureFromSurface( renderer, textSurface );		
        if( mTexture == NULL )
        {
            printf( "Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError() );						
        }
        else
        {
			// set meta information
			mname = font_path;							

			mtype = TTF;			

            //Get image dimensions
            mSpritesheetWidth = textSurface->w;
            mSpritesheetHeight = textSurface->h;			
        }

        //Get rid of old surface
        SDL_FreeSurface( textSurface );
    }
    
    //Return success
    return mTexture != NULL;
}

bool Texture::loadFromRenderedText( SDL_Renderer* sdl_renderer, TTF_Font* font, string font_path, int index, int textureText, SDL_Color textColor )
{
	return loadFromRenderedText(sdl_renderer, font, font_path, index, to_string(textureText), textColor);
}

bool Texture::loadFromRenderedText( SDL_Renderer* sdl_renderer, TTF_Font* font, string font_path, int index, double textureText, SDL_Color textColor )
{
	return loadFromRenderedText(sdl_renderer, font, font_path, index, to_string(textureText), textColor);
}

bool Texture::loadFromRenderedText( SDL_Renderer* sdl_renderer, TTF_Font* font, string font_path, int index, float textureText, SDL_Color textColor )
{
	return loadFromRenderedText(sdl_renderer, font, font_path, index, to_string(textureText), textColor);
}

void Texture::deserialize(vector<string> metaInfo, SDL_Texture* texture, SDL_Renderer* sdl_renderer)
{
	free();

	//set renderer
	renderer = sdl_renderer;
	
	mTexture = texture;
	mname = metaInfo[0];
	
	// clip info
	mClipWidth = stoi(metaInfo[1]);
	mClipHeight = stoi(metaInfo[2]);
	
	//Get sprite sheet dimensions
	SDL_QueryTexture( mTexture, NULL, NULL, &mSpritesheetWidth, &mSpritesheetHeight );				
	
	// set texture type
	mtype = PNG;										

	// set sprite sheet width / height	
	{
		int widthCount = mSpritesheetWidth / mClipWidth;
		int heightCount = mSpritesheetHeight / mClipHeight;	

		for (int i = 0; i < widthCount; i++)
		{
			for ( int j = 0; j < heightCount; j++ )
			{
				SDL_Rect rect = { i * mClipWidth, j * mClipHeight, mClipWidth, mClipHeight };			
				mSpriteClips.push_back(rect);
			}
		}
		mCurrentClipIndex = 0;
	}							

	// set pivot position
	mPivotPosition.x = mClipWidth / 2;
	mPivotPosition.y = mClipHeight / 2;		

	printf ("deserialize success: %s, ( %i, %i )\n", mname.c_str(), mClipWidth, mClipHeight);	
}

void Texture::render( double angle, SDL_RendererFlip flip, bool update_animation )
{
	//Set rendering space and render to screen	
	SDL_Rect renderQuad = { mPosition.x, mPosition.y, mClipWidth, mClipHeight };

	if ( mtype == PNG )
	{		
		if ( mSpriteClips.size() > 0 && mCurrentClipIndex < mSpriteClips.size() )
		{		
			// printf( "%s: ( %i, %i )\n", mname.c_str(), mSpriteClips[mCurrentClipIndex].w, mSpriteClips[mCurrentClipIndex].h );										
			SDL_RenderCopyEx( renderer, mTexture, &mSpriteClips[mCurrentClipIndex], &renderQuad, angle, &mPivotPosition, flip );			
		}

		if (update_animation)
		{
			mCurrentClipIndex++;
			mCurrentClipIndex = mCurrentClipIndex % mSpriteClips.size();
		}
	}
	else if ( mtype == TTF )
	{	
		SDL_RenderCopy( renderer, mTexture, NULL, &renderQuad);		
	}
}

void Texture::free()
{
	//Free texture if it exists
	if( mTexture != NULL )
	{		
		mTexture = NULL;
		mSpritesheetWidth = 0;
		mSpritesheetHeight = 0;
	}

	// free mSpriteClips?	
	mSpriteClips.clear(); // calls destructor for SDL_Rect
	mSpriteClips.shrink_to_fit(); // set capacity to 0, gurantee memory deallocation
}

void Texture::setSpriteClipIndex(int index)
{	
	mCurrentClipIndex = index % mSpriteClips.size();
}