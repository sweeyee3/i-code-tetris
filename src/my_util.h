#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

#pragma once
namespace my_util
{
    void print_v_s(vector<string> a);
    void print_v_i(vector<int> a);
    void print_v_f(vector<float> a);
    void print_v_d(vector<double> a);    
    void print_v_c(vector<char> a);
    vector<string> splitBy(string line, char delimiter);
}