#include "block.h"
#include <SDL2/SDL.h>
#include "loader.h"

using namespace std;

const int CONTAINER_HEIGHT = 21;
const int CONTAINER_WIDTH = 10;
// const int MAX_CONTAINER_HEIGHT = CONTAINER_HEIGHT - 3;
const int MAX_CONTAINER_WIDTH = CONTAINER_WIDTH - 3;
const int START_X = 320;
const int START_Y = 160;
const int MOVE_TIMER = 500; // milliseconds

#pragma once
class BlockContainer {
    public:
        int container [CONTAINER_HEIGHT][CONTAINER_WIDTH];
        int clear[CONTAINER_HEIGHT];
        Block* current;
        Block* next; // spawn next block for display
        int screenWidth;
        int screenHeight;
        Uint32 updateTimer;
        int speedTimer;
        bool gameOver;

        ~BlockContainer() 
        {
            delete current;
        } // destructor

        void spawn_block();

        void find_final_pos();
        void set_final_pos();
        
        bool update_container(int flag);
        
        void check_clear_row();
        void move_rows();
        void move_one_row(int from, int to);

        void right();
        void left();
        void rotate(int direction);
        void speed(int newSpeed);
        
        bool is_overlap(Block* current);
        bool is_out_upper_x_bounds(Block* block); // if int is -1, do not check
        bool is_out_upper_y_bounds(Block* block); // if int is -1, do not check
        
        bool is_out_lower_x_bounds(Block* block); // if int is -1, do not check
        bool is_out_lower_y_bounds(Block* block); // if int is -1, do not check

        void initialize(int width, int height);
        void update();
        void render(SDL_Renderer* renderer);
};