#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include "block_container.h"
#include "timer.h"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

bool init();
bool load();
void close();
void update();
void render();

SDL_Window* gWindow = NULL;
SDL_Renderer* gRenderer = NULL;
SDL_Event* gEventInput;

BlockContainer* gContainer;

bool gQuit = false;

bool init() {
    bool success = true;
    if (SDL_Init( SDL_INIT_VIDEO|SDL_INIT_TIMER ) < 0)
    {
        printf("SDK could not initialize, SDL_ERROR: %s\n", SDL_GetError());
        success = false;
    }
    else
    {
        gWindow = SDL_CreateWindow( "i-code-tetris", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN );
        if (gWindow == NULL)
        {
            printf("Window could not be created, SDL_ERROR: %s\n", SDL_GetError());
            success = false;
        }
        else
        {
            // create renderer
            gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED ); // uses GPU to display image instead of CPU (surfaces)
            if ( gRenderer == NULL )
            {
                printf("Renderer could not be creaed, SDL_ERROR: %s\n", SDL_GetError());
                success = false;                
            }
            else
            {
                gEventInput = new SDL_Event();
                // renderer color
                SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0xFF );

                // load png images
                int imgFlags = IMG_INIT_PNG;
                if ( !(IMG_Init( imgFlags ) & imgFlags ) )
                {
                    printf( "SDL_Image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
                    success = false;
                }

                // load fonts (ttf)
                if ( TTF_Init() == -1 )
                {
                    printf("SDL_ttf, could not initialize! SDL_Error: %s\n", TTF_GetError());
                    success = false;
                }
            }
        }
    }
    printf("%d\n", success);
    return success;    
}

void close()
{
    delete gContainer;

    SDL_DestroyRenderer( gRenderer );
    gRenderer = NULL;

    delete gEventInput;
    gEventInput = NULL;

    SDL_DestroyWindow( gWindow );
    gWindow = NULL;

    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

void render()
{
    SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0x00 );
    SDL_RenderClear( gRenderer );
    
    gContainer->render(gRenderer);

    SDL_RenderPresent( gRenderer );
}

void update()
{
    Timer::instance()->update();
    gContainer->update();

    while( SDL_PollEvent( gEventInput ) != 0 ) // NOTE: this is needed for SDL to create window
    {        
        //User requests quit
        if( gEventInput->type == SDL_QUIT )
        {
            gQuit = true;
            break;
        }
        if (gEventInput->type == SDL_KEYDOWN)
        {
            switch (gEventInput->key.keysym.sym)
            {
                case SDLK_RIGHT:
                gContainer->right();
                break;
                case SDLK_LEFT:
                gContainer->left();
                break;
                case SDLK_UP:
                gContainer->rotate(1);
                break;
                case SDLK_DOWN:
                gContainer->speed(10);
                break;
            }                        
        }

        if (gEventInput->type == SDL_KEYUP)
        {
            switch (gEventInput->key.keysym.sym)
            {
                case SDLK_DOWN:
                gContainer->speed(-1);
                break;
            }
        }                                     
    }    
}

bool load()
{
    // TODO: Load spritesheet / fonts / sounds here
    Loader::instance()->loadTextureFile( "resources/img", gRenderer );

    Loader::instance()->loadFontFile("resources/font", gRenderer);

    Loader::instance()->loadMetaTextureFile("resources/img/img.mtex", gRenderer);

    gContainer = new BlockContainer();

    gContainer->initialize(SCREEN_WIDTH, SCREEN_HEIGHT);

    Timer::instance()->start();

    return true;
}

int main(int argc, char *argv[]) {
    if (init())
    {
        if (load())
        {
            while (!gQuit)
            {                
                update();
                render();
            }
        }
        else
        {
            printf("Failed to load\n");
        }        
    }
    close();
    return 0;        
}