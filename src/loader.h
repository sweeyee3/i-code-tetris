#include <string>
#include <sstream>
#include <map>
#include <filesystem>
#include <iostream>
#include <fstream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "SDL_FontCache.h"
#include "texture.h"
#include "my_util.h"
// #include "SGL_InputHandler.h"
// #include "SGL_Constants.h"
// #include "SGL_Scene.h"
// #include "DearImGui/imgui.h"
using namespace std;

// Load game asset
class Loader
{
    public:
        static Loader *instance()
        {
            if (!m_instance)
            {
                m_instance = new Loader();
            }
            return m_instance;
        }

        map<string, SDL_Texture*> textureMap;
        map<string, Texture*> textureWrapperMap;

        map<string, FC_Font*> fontMap;

        // string debug_selected_texture_id;
        // bool is_new_selected_texture;        

        // input loading
        bool loadTextureFile(string dir_path, SDL_Renderer* renderer);        
        bool loadMetaTextureFile( string meta_file_path, SDL_Renderer* renderer);

        bool loadFontFile(string dir_path, SDL_Renderer* renderer);        

        bool loadSoundFile(string dir_path, string meta_file_path);

        // data loading
        // bool loadSceneFile(string file_path);                                    

        // asset loading
        bool loadTexture(SDL_Renderer* sdl_renderer, string image_path);
        bool loadFonts(string dir_path, SDL_Renderer* renderer);
        bool loadSound();
        
        // data loading
        bool loadTextureWrapper();
        // bool loadBoundingBox();
        // bool loadScene(string dir_path, LScene* scene);

        /* SAVE */
        // bool saveMetaTextureFile(string data, string meta_file_path);                
        // bool saveScene(string data, string dir_path);

        void update();        
        // void fixedUpdate();
        void render(SDL_Renderer* renderer);        

        void free();

        // debug
        // void debug_update();
        // void debug_imgui_update();
        // void debug_render(FC_Font* font, SDL_Renderer* renderer);

    private:
        // constructor
        Loader() 
        {
            // debug_selected_texture_id = "";
            // is_new_selected_texture = false;                        
        };                

        // destructor
        ~Loader() 
        {
            free();
        };

        static Loader *m_instance;
};