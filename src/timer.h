#ifndef SGL_TIMER_H
#define SGL_TIMER_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
using namespace std;

class Timer
{
    public:
        static Timer *instance()
        {
            if (!m_instance)
            {
                m_instance = new Timer();
            }
            return m_instance;
        }        
        
        // stores total number of frames        
        uint mTotalFrames;

        Uint32 getTicks();
        
        void start();
        void stop();
        void pause();
        void unpause();

        bool isStarted();
        bool isPaused();
        bool isTimePassed(Uint32 a, Uint32 b);        

        void update();

        void free();

    private:
        Timer() 
        {
            //Initialize the variables
            mStartTicks = 0;
            mPausedTicks = 0;

            mPaused = false;
            mStarted = false;
        };
        ~Timer() {};

        static Timer *m_instance;
                
        //The clock time when the timer started
        Uint32 mStartTicks;

        //The ticks stored when the timer was paused
        Uint32 mPausedTicks;        

        //The timer status
        bool mPaused;
        bool mStarted;                      
};
#endif